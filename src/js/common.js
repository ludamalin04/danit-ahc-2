let btn = document.querySelector('.header__btn');
let list = document.querySelector('.header__list');

btn.addEventListener('click', function () {
    btn.classList.toggle('active');
    list.classList.toggle('visible');
})